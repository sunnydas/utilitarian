![Lets build stuff!](https://bitbucket.org/sunnydas/utilitarian/raw/741e81878494a2023a69de002368b262b8e6a1d9/images/action-2277292_640.jpg)

- - - 

Project Utilitarian
====================

This project aims to build an assortment of recipes in different languages to solve some common problems, provide
workarounds and hacks. We can think of this a project that included small utilities that can be used freely.
This project is available under MIT license.
- - -

Building the project
====================

   * Clone or fork the project.
   * This is a gradle based project, so you can issue:
     gradle (which will run compile, run the tests and generate the final package)
   * The package will be available in the "build/dist" directory  

- - -

Utilities
===========
   * Bash    
      * [dos2unix for zip files](https://bitbucket.org/sunnydas/utilitarian/src/master/src/main/bash/dos2unixZip/)
      * [Simple HTTP health checker utility](https://bitbucket.org/sunnydas/utilitarian/src/master/src/main/bash/httpHealthChecker/)
      * [Simple way of extracting numeric value from a json based file](https://bitbucket.org/sunnydas/utilitarian/src/master/src/main/bash/parse_numeric_value_for_attribute_json/parse_numeric_value_json.sh)
   * Java
      * [Get file sizes in bytes,kilobytes,megabytes or gigabytes](https://bitbucket.org/sunnydas/utilitarian/src/master/src/main/java/com/utilitarian/utils/base/file/io/FileUtils.java)
      * [Get directory sizes in bytes,kilobytes,megabytes or gigabytes](https://bitbucket.org/sunnydas/utilitarian/src/master/src/main/java/com/utilitarian/utils/base/file/io/FileUtils.java)
      * [Generate a text file of given size](https://bitbucket.org/sunnydas/utilitarian/src/master/src/main/java/com/utilitarian/utils/base/file/io/FileUtils.java)
      * [Merge multiple files into a single file](https://bitbucket.org/sunnydas/utilitarian/src/master/src/main/java/com/utilitarian/utils/base/file/io/FileUtils.java)
   * Python
      * [Check port occupancy status](https://bitbucket.org/sunnydas/utilitarian/src/master/src/main/python/port_available_status.py)
      * [Simple Sentiment Analyser](https://bitbucket.org/sunnydas/utilitarian/src/master/src/main/python/sentiment_analyser/sentiment_analyser.py)
	   
- - -
