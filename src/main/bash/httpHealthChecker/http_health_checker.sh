#!/usr/bin/env bash

#########################################################################
# @description: This script can be used to perform simple health checks #                         
# (htttp/https).                                                        #  
# @author: Sunny Das                                                    #
# @license: MIT                                                         #
#########################################################################

RED='\033[0;31m'
GREEN='\033[0;32m'
NO_COLOR='\033[0m'
help(){
   echo "===================================="
   echo "Simple http/https health checker.We can think of this as a simple pinger."
   echo "Usage: ./http_health_checker.sh -u <url>"
}


set_config(){
  if [ -z "$UTILITARIAN_POLL_INTERVAL" ];then
     UTILITARIAN_POLL_INTERVAL=30 
  fi
  if [ -z "$UTILITARIAN_POLL_COUNT" ];then
     UTILITARIAN_POLL_COUNT=10
  fi 
}

check_health(){
   url="$OPTARG"
   echo "HTTP/HTTPS Health checker utility"
   if [ -z "$url" ];then
     echo "Url cannot be blank."
     exit 1 
   fi
   echo "ping url is : $url"
   url="${url,,}"
   command -v curl >/dev/null 2>&1 || { echo >&2 "Required module curl is not installed.  Aborting."; exit 1; }
   set_config
   counter="$UTILITARIAN_POLL_INTERVAL"
   while [ "$counter" -gt 0 ];do
     status=`curl -k -s -o /dev/null -L -I -w "%{http_code}" "$url"`
     if [ "$?" -gt 0 ];then
        echo -e "${RED}ERROR${NO_COLOR}"
     else
          echo -e "${GREEN}HEALTHY${NO_COLOR}"
     fi
     sleep $UTILITARIAN_POLL_INTERVAL
     let counter-=1 
   done
}


OPTIND=1
while getopts "h?u:" opt; do
    case "$opt" in
    h|\?)
        help
        exit 0
        ;;
    u)
     check_health 
     exit 0
     ;;
    *)
      help
      exit 0
    ;;
    esac
done
shift $((OPTIND-1))

