#!/usr/bin/env bash

#########################################################################
# @description: This script can be used to run                          #
# the dos2unix tool for zip files.                                      #  
# @author: Sunny Das                                                    #
# @license: MIT                                                         #
#########################################################################


dos2UnixInvoke(){
   fileName="$OPTARG"
   command -v dos2unix >/dev/null 2>&1 || { echo >&2 "Required module dos2unix is not installed.  Aborting."; exit 1; }
   if [ ! -f  $fileName ]; then
    echo "File = $fileName does not exist,aborting"
    exit 1
   fi
   checkCommandValid "dos2unix"
   checkCommandValid "file"
   isZip=`file $fileName | grep zip | wc -l`
   if [ "$isZip" -gt 0 ];then
      echo "Running do2unix for zip file = $fileName"
      dos2UnixZip $fileName
   else
      echo "Running dos2unix on $fileName"
      dos2unix $fileName
   fi 
   echo "Done execution."
}

checkCommandValid(){
  cmd="$1"
  command -v $cmd >/dev/null 2>&1 || { echo >&2 "Required module $cmd is not installed.  Aborting."; exit 1; }
}


dos2UnixZip(){
  zipFileName="$1"
  checkCommandValid "basename"
  zipBaseName=`basename $zipFileName`
  echo "zip base name is $zipBaseName"
  if [ -z "$tempDir" ];then
     tempDir="$HOME/temp"
  fi  
  echo "Using temp staging dir as $tempDir"
  if [ ! -d  $tempDir ]; then
    echo "Directory = $tempDir does not exist,creating"
    mkdir -p $tempDir
  fi 
  timeStamp=`date +%s%N | cut -b1-13`
  stageDir="$tempDir/project_utilitarian/stage/dos2unix_zip_$timeStamp"
  mkdir -p $stageDir
  checkCommandValid "unzip"
  echo "Unzipping to $stageDir"
  unzip "$zipFileName" -d "$stageDir"
  curDir=`pwd`
  cd $stageDir
  echo "running dos2unix"
  find . -name "*.*" -exec dos2unix {}  \;
  #sudo chmod -R 755 *
  ls -ltr 
  checkCommandValid "zip"
  echo "Zipping back the files"
  zip -r "$zipBaseName"  .
  mkdir -p "$curDir/dist"
  cp $zipBaseName $curDir/dist
  if [ $? -eq 0 ];then
     cd $curDir
     rm -rf $stageDir
     echo "Converted zip is in dist folder"
     ls -ltr dist
  else
     echo "Conversion failed"
     exit 1 
  fi  
}

help(){
   echo "===================================="
   echo "Run dos2unix on zip files.(works on normal files as well)"
   echo "Usage: ./dos2unix_zip.sh -f <file_name>"
}


OPTIND=1
while getopts "h?f:" opt; do
    case "$opt" in
    h|\?)
        help
        exit 0
        ;;
    f) 
     dos2UnixInvoke $OPTARG
     exit 0
     ;; 
    *)
      help
      exit 0
    ;;      
    esac
done
shift $((OPTIND-1))
