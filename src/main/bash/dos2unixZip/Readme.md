Utility: This script tries to run dos2unix on zip files (works on normal file as well).

Usage:
   ./dos2unix_zip.sh -f <file_name>
   
Help: 
  ./dos2unix_zip.sh -h 
   
Limitation:
  Not yet supported for gzip files.     