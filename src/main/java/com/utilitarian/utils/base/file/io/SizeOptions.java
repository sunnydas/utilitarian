package com.utilitarian.utils.base.file.io;

/**
 * This class represents the various size options to calculate file sizes (for example : bytes,kb,mb)
 *
 * @author Sunny Das
 */
public enum SizeOptions {
    BYTES,
    KILOBYTES,
    MEGABYTES,
    GIGABYTES
}
