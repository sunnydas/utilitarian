import argparse
from beautifultable import BeautifulTable
from textblob.classifiers import NaiveBayesClassifier
from textblob import TextBlob

welcome="""
  ==============================================================
  
  Simple Sentiment Analyser.
  
  Version : 1.0 
  
  ==============================================================
"""

def analyseSentiment(input_text):
    sentiment=TextBlob(input_text).sentiment
    print "\033[1;37;40m "     
    table = BeautifulTable()
    print "Sentiment analysis report:"
    table.column_headers = ["Attribute", "Value"]
    table.append_row(["polarity",sentiment.polarity])
    table.append_row(["subjectivity",sentiment.subjectivity])
    print table
		 
def checkInvalidInput(args):
  if not args.input_text:
     print "\033[31;1m [Error] Input text is not specified."
     sys.exit(1) 
		 
print "\033[36;1m " + welcome
parser = argparse.ArgumentParser(description="Simple Sentiment Analyser Illustration.")
parser.add_argument("-t","--input_text", help="The input text")
args = parser.parse_args()
if args.input_text:
  checkInvalidInput(args)
  analyseSentiment(args.input_text)
else:
  print "\033[31;1m [Error] Invalid usage.Type -h for help"   