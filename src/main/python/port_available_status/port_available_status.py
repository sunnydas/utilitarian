import socket 
import errno
import argparse


#########################################################################
# @description: This script can be used to check port avail status      #
#                                                                       #
# @author: Sunny Das                                                    #
# @license: MIT                                                         #
#########################################################################


def checkPortAvailability(port,ip):
  available = "yes"
  if port:
     sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
     sock.settimeout(10)
     try:
       status = sock.connect_ex((ip, int(port)))
       print "Connection status is : " + str(status)
       if status == 0:
          available = "no"
     except socket.error as e:
          print "Error checking availability"
          print(e)
          available = "error"
  return available


print "Port availability checker , tested on Python 2.7.12"
parser = argparse.ArgumentParser()
parser.add_argument('-p', '--port',required=True)
parser.add_argument('-a', '--address', default="127.0.0.1")
args = parser.parse_args()
port = args.port
ip = args.address
status = checkPortAvailability(port,ip)
print "Port availability status = " + status


